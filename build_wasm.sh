#! /usr/bin/env bash

set -ex

cargo update
TARGET=target/wasm32-unknown-unknown/debug
cargo build --features=fail-on-warnings --target wasm32-unknown-unknown
rm -rf wasms
wasm-bindgen --out-name bindgens.wasm --out-dir ./wasms --target no-modules ${TARGET}/zingo_webextension.wasm
rm -f src/bindgens.js
mv wasms/bindgens.js src/

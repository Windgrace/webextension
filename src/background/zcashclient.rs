use pairing::bls12_381::Bls12;
use zcash_client_backend::{
    constants::testnet::HRP_SAPLING_PAYMENT_ADDRESS, encoding::encode_payment_address,
};
use zcash_primitives::keys::OutgoingViewingKey;
use zcash_primitives::primitives::{Diversifier, PaymentAddress};
use zcash_primitives::zip32::{ExtendedFullViewingKey, ExtendedSpendingKey};
use zcash_primitives::JUBJUB;

pub struct Client {
    address: PaymentAddress<Bls12>,
}

impl Client {
    pub fn new() -> Self {
        let extsk = ExtendedSpendingKey::master(&[0; 32]);
        let extfvk = ExtendedFullViewingKey::from(&extsk);
        let address = extfvk.default_address().unwrap().1;

        Client { address }
    }

    pub fn address(&self) -> String {
        encode_payment_address(HRP_SAPLING_PAYMENT_ADDRESS, &self.address)
    }
}

use ff::Field;

pub struct RngFromBrowser {
    pub random_values: [u8; 16],
}

impl RngFromBrowser {
    pub fn new() -> Self {
        let mut key = [0u8; 16];
        let window = web_sys::window().expect("We shoudl be able to access window");
        let crypto_interface = window
            .crypto()
            .expect("should be able to access the global crypto");
        let js_sys_obj_values =
            web_sys::Crypto::get_random_values_with_u8_array(&crypto_interface, &mut key)
                .expect("the browser should have generated random numbers");
        let mut random_values = [0u8; 16];
        for (index, rand) in js_sys::Object::values(&js_sys_obj_values)
            .values()
            .into_iter()
            .enumerate()
        {
            random_values[index] = rand
                .expect("should have found a random value!")
                .as_f64()
                .expect("should have been an f64") as u8;
        }
        RngFromBrowser { random_values }
    }
}

impl rand_core::CryptoRng for RngFromBrowser {}
impl rand_core::RngCore for RngFromBrowser {
    fn next_u32(&mut self) -> u32 {
        self.random_values[0] as u32
    }
    fn next_u64(&mut self) -> u64 {
        self.random_values[0] as u64
    }
    fn fill_bytes(&mut self, target: &mut [u8]) {
        for (index, rand) in self.random_values.iter().enumerate() {
            target[index] = *rand;
        }
    }
    fn try_fill_bytes(&mut self, target: &mut [u8]) -> std::result::Result<(), rand_core::Error> {
        Ok(self.fill_bytes(target))
    }
}

pub struct NoteMaker {}
impl NoteMaker {
    pub fn create_note() {
        let diversifier = Diversifier([0; 11]);
        let pk_d = diversifier.g_d::<Bls12>(&JUBJUB).unwrap();
        let to = PaymentAddress::from_parts(diversifier, pk_d).unwrap();
        let ovk = OutgoingViewingKey([0; 32]);
        let value = 1000;
        let mut rng = RngFromBrowser::new();
        use zcash_primitives::jubjub::fs;
        let rcv = fs::Fs::random(&mut rng);
        use zcash_primitives::primitives;
        let cv = primitives::ValueCommitment::<Bls12> {
            value,
            randomness: rcv.clone(),
        };
        let note = to.create_note(value, rcv, &JUBJUB).unwrap();
        let cmu = note.cm(&JUBJUB);
        let ascii_only_example = "TESTtest101";

        let mut ascii_example_bytes = [0u8; 11];
        for (index, byte) in ascii_only_example.bytes().enumerate() {
            ascii_example_bytes[index] = byte;
        }
        use zcash_primitives::note_encryption;
        let enc = note_encryption::SaplingNoteEncryption::new(
            ovk,
            note,
            to,
            note_encryption::Memo::from_bytes(&ascii_example_bytes).expect("need memo!"),
            &mut rng,
        );
        let _enc_ciphertext = enc.encrypt_note_plaintext();
        let _out_ciphertext = enc.encrypt_outgoing_plaintext(&cv.cm(&JUBJUB).into(), &cmu);
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    use wasm_bindgen_test::{wasm_bindgen_test, wasm_bindgen_test_configure};

    wasm_bindgen_test_configure!(run_in_browser);
    #[wasm_bindgen_test]
    fn stub() {
        assert_eq!(2, 2);
    }
}

use wasm_bindgen::closure::Closure;
use wasm_bindgen::{JsCast, JsValue};
use wasm_bindgen_futures::JsFuture;
use wasm_bindgen_macro::wasm_bindgen;
use web_sys;
mod curationdb;

use cfg_if;

mod mock;
use crate::mock_in_test;
mock_in_test!(web_extension);

#[wasm_bindgen]
pub async fn content() {
    console_error_panic_hook::set_once();
    let runtime = &(*web_extension::browser).runtime();
    get_zaddr(runtime).await;
    let dom_manip = move || {
        run_reordering_logic();
    };
    let window = web_sys::window().expect("no global window object");
    let document = window.document().expect("window has no document");
    match document.ready_state().as_str() {
        "complete" => dom_manip(),
        _ => window.set_onload(Some(
            Closure::once_into_js(dom_manip).as_ref().unchecked_ref(),
        )),
    };
    ()
}

fn add_weights_to_results(results: &Vec<(web_sys::Element, u32)>) -> Vec<(&web_sys::Element, u32)> {
    let local_db = curationdb::load_curation_db();
    let mut reordered_results = Vec::new();
    results.iter().for_each(|(element, _zero)| {
        let weight = match local_db.get(&find_url(element)) {
            Some(value) => {
                add_zingo_icon(element);
                *value as u32
            }
            None => 0,
        };
        reordered_results.push((element, weight));
    });
    reordered_results
}

fn reorder_results_by_weight<'a>(
    mut reordered_results: Vec<(&'a web_sys::Element, u32)>,
) -> Vec<&'a web_sys::Element> {
    reordered_results.sort_by(|(_e1, weight1), (_e2, weight2)| weight2.cmp(weight1));
    reordered_results
        .iter()
        .map(|(element, _weight)| *element)
        .collect()
}

fn find_url(element: &web_sys::Element) -> String {
    element
        .first_child_of_class("result__body links_main links_deep")
        .first_child_of_class("result__extras js-result-extras")
        .first_child_of_class("result__extras__url")
        .text_content()
        .expect("No url on element")
}
trait ChildrenByClass {
    fn first_child_of_class(&self, class: &str) -> web_sys::Element;
}

impl ChildrenByClass for web_sys::Element {
    fn first_child_of_class(&self, class: &str) -> web_sys::Element {
        for child in 0..self.children().length() {
            if self
                .children()
                .item(child)
                .expect("child list shorter than expected")
                .class_name()
                == class
            {
                return self
                    .children()
                    .item(child)
                    .expect("Child removed during execution");
            }
        }
        panic!("No child of class {}", class);
    }
}

fn get_results_element(document: &web_sys::Document) -> web_sys::Element {
    document
        .get_elements_by_class_name("results")
        .item(0)
        .expect("results element does not exist")
}

fn append_reordered_results_to_dom(
    ddg_results_element: web_sys::Element,
    reordered_results: &Vec<&web_sys::Element>,
) {
    reordered_results.iter().for_each(|x| {
        ddg_results_element
            .append_child(&x)
            .expect("Child appending failed");
    });
    ddg_results_element
        .append_child(
            &ddg_results_element
                .children()
                .named_item("rld-1")
                .expect("No \"More Results\" element"),
        )
        .expect("failed to append child");
}

async fn get_zaddr(runtime: &web_extension::Runtime) {
    let ear = Closure::wrap(Box::new(move |message: &JsValue| {
        web_sys::console::log_1(message);
    }) as Box<dyn Fn(&JsValue)>);
    runtime
        .on_message()
        .add_listener(ear.as_ref().unchecked_ref());
    ear.forget();
    match JsFuture::from(runtime.send_message(None, &JsValue::from_str("active"), None)).await {
        Ok(x) => web_sys::console::log_2(&JsValue::from("ok"), &x),
        Err(e) => web_sys::console::log_2(&JsValue::from("err"), &JsValue::from(e)),
    };
}

fn add_zingo_icon(element: &JsValue) {
    let window = web_sys::window().expect("no global window object");
    let document = window.document().expect("window has no document");
    let runtime = &(*web_extension::browser).runtime();
    let icon = web_sys::HtmlMediaElement::from(JsValue::from(
        document
            .create_element("img")
            .expect("failed to create child"),
    ));
    icon.set_src(
        runtime
            .get_url("icons/zingo32px.png")
            .as_string()
            .expect("not a string")
            .as_str(),
    );
    icon.set_attribute("height", "32")
        .expect("failed to set height");
    icon.set_attribute("width", "32")
        .expect("failed to set width");
    web_sys::Element::from(element.clone())
        .append_child(&icon)
        .expect("failed to append child!");
}

fn get_search_results(ddg_results_element: &web_sys::Element) -> Vec<(web_sys::Element, u32)> {
    let mut result_vec = Vec::new();
    for i in 0..ddg_results_element.children().length() {
        if ddg_results_element
            .children()
            .item(i)
            .expect("results shorter than expected")
            .id()
            .starts_with("r1-")
        {
            result_vec.push((
                ddg_results_element
                    .children()
                    .item(i)
                    .expect("Results shorter than expected. This should be unreachable."),
                0,
            ));
        }
    }
    result_vec
}

fn run_reordering_logic() {
    let window = web_sys::window().expect("no global window object");
    let document = window.document().expect("window has no document");
    let ddg_results_element = get_results_element(&document);
    let search_results = get_search_results(&ddg_results_element);
    let weighted_results = add_weights_to_results(&search_results);
    let reordered_results = reorder_results_by_weight(weighted_results);
    append_reordered_results_to_dom(ddg_results_element, &reordered_results);
}

#[cfg(test)]
mod tests {
    use super::*;
    use wasm_bindgen_test::{wasm_bindgen_test, wasm_bindgen_test_configure};
    wasm_bindgen_test_configure!(run_in_browser);

    #[wasm_bindgen_test]
    async fn test_get_zaddr() {
        let runtime = &(*web_extension::browser).runtime();
        get_zaddr(runtime).await;
    }
}

use std::collections::HashMap;

pub(crate) fn load_curation_db() -> HashMap<String, u64> {
    let mut curation_db: HashMap<String, u64> = HashMap::new();
    curation_db.insert(String::from("https://www.youtube.com/user/foofighters"), 1);
    curation_db.insert(
        String::from("https://www.urbandictionary.com/define.php?term=foo"),
        2,
    );
    curation_db
}

#[allow(warnings)]
pub(crate) mod web_extension {
    use mockall::automock;
    use std::ops;
    use wasm_bindgen::JsValue;
    pub struct Event {}
    impl Event {
        pub fn add_listener(&self, ear: &JsValue) {}
    }
    pub struct Runtime {}
    #[automock]
    impl Runtime {
        pub fn send_message(
            &self,
            target: Option<String>,
            message: &JsValue,
            options: Option<String>,
        ) -> js_sys::Promise {
            js_sys::Promise::resolve(&JsValue::from(""))
        }
        pub fn on_message(&self) -> Event {
            Event {}
        }
        pub fn get_url(&self, location: &str) -> JsValue {
            JsValue::from("MOCKED")
        }
    }
    pub struct BrowserCore {}
    impl BrowserCore {
        pub fn runtime(&self) -> Runtime {
            Runtime {}
        }
    }
    pub struct Browser {}
    pub static browser: self::Browser = Browser {};

    impl ops::Deref for Browser {
        type Target = BrowserCore;
        fn deref(&self) -> &BrowserCore {
            &BrowserCore {}
        }
    }
}

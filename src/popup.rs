use js_sys::{Array, Object};
use wasm_bindgen::closure::Closure;
use wasm_bindgen::{JsCast, JsValue};
use wasm_bindgen_futures::JsFuture;
use wasm_bindgen_macro::wasm_bindgen;
use web_extension;
use web_sys;

#[wasm_bindgen]
pub async fn popup() {
    console_error_panic_hook::set_once();
    let url = get_active_tab_url().await;
    let window = web_sys::window().expect("no popup window object");
    let document = window.document().expect("popup window has no document");
    let button = web_sys::HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("endorse")
            .expect("no endorsement button"),
    ));
    let endorse = Closure::once_into_js(move || {
        let runtime = &(*web_extension::browser).runtime();
        web_sys::console::log_1(&runtime.send_message(None, &JsValue::from(url), None));
        window.close().expect("Failed to close the window!");
    });
    button.set_onclick(Some(endorse.as_ref().unchecked_ref()));
}

fn make_pair_array<T, U>(element_one: T, element_two: U) -> js_sys::Array
where
    JsValue: From<T> + From<U>,
{
    use std::iter::FromIterator;
    Array::from_iter(vec![
        &JsValue::from(element_one),
        &JsValue::from(element_two),
    ])
}

async fn get_active_tab_url() -> String {
    let tabs = web_extension::Browser::tabs(&*web_extension::browser);
    let active = make_pair_array("active", true);
    let current_window = make_pair_array("currentWindow", true);
    let query = make_pair_array(active, current_window);
    let result =
        JsFuture::from(tabs.query(&Object::from_entries(&query).expect("no tab found"))).await;
    web_extension::Tab::from(js_sys::Array::from(&result.expect("Can't find current tab")).get(0))
        .url()
        .expect("no url for tab")
}

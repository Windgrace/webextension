use wasm_bindgen::closure::Closure;
use wasm_bindgen::{JsCast, JsValue};
use wasm_bindgen_macro::wasm_bindgen;
use web_extension;
use web_sys;

mod zcashclient;
#[wasm_bindgen]
pub async fn background() {
    console_error_panic_hook::set_once();
    let runtime = web_extension::Browser::runtime(&*web_extension::browser);
    let address = zcashclient::Client::new().address();
    web_sys::console::log_1(&"background is in".into());
    let receiver = Closure::wrap(Box::new(move |message: JsValue, sender: JsValue| {
        web_sys::console::log_2(&JsValue::from("received"), &message);
        web_sys::console::log_2(&JsValue::from("sender:"), &sender);
        let m: String = message
            .as_string()
            .expect("Message can not be read as string");
        if m.as_str() == "active" {
            let tabs = web_extension::Browser::tabs(&*web_extension::browser);
            web_sys::console::log_1(
                &tabs.send_message(
                    web_extension::MessageSender::from(sender)
                        .tab()
                        .expect("\"Active\" message was not sent from a tab")
                        .id()
                        .expect("Tab has nonexistent tab ID"),
                    &JsValue::from_str(&address),
                    None,
                ),
            );
        } else {
            web_sys::console::log_1(&JsValue::from("Not a recognized input"));
        }
    }) as Box<dyn Fn(JsValue, JsValue)>);
    runtime
        .on_message()
        .add_listener(receiver.as_ref().unchecked_ref());
    receiver.forget();
    zcashclient::NoteMaker::create_note();
    let rngfb = zcashclient::RngFromBrowser::new();
    web_sys::console::log_1(&JsValue::from(format!("{:?}", rngfb.random_values)));
}

#[wasm_bindgen]
pub fn foo() -> u32 {
    1
}
